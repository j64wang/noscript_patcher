#!/usr/bin/env python3

import sys
import subprocess
import os
import requests
import zipfile
import shutil
import tempfile

NOSCRIPT_DL_PAGE = "https://noscript.net/getit"
BUILD_DIR = "build"
LINK_PREFIX = b"https://secure.informaction.com/download/releases/noscript-"
FILE_PREFIX = "noscript-"
FILE_SUFFIX = ".xpi"

def main():
    response = requests.get(NOSCRIPT_DL_PAGE)
    if not response.ok:
        print("HTTP request to " + NOSCRIPT_DL_PAGE + " failed with code " + response.status_code)
        return 2
    link_idx = response.content.find(LINK_PREFIX)
    if link_idx == -1:
        print("Could not find download link in HTML")
        return 3
    end_idx = response.content.find(b"\"",link_idx + len(LINK_PREFIX))
    if end_idx == -1:
        print("Could not find closing quote of link")
        return 4
    link = response.content[link_idx:end_idx]
    response = requests.get(link,stream = True)
    if not response.ok:
        print("HTTP download request to " + link + " failed with code " + response.status_code)
        return 5
    if os.path.exists(BUILD_DIR):
        print("Build directory(" + BUILD_DIR + ") already exists!")
        return 6
    os.mkdir(BUILD_DIR)
    response_xpi = tempfile.TemporaryFile()
    response_xpi.write(response.content)
    zipfile.ZipFile(response_xpi).extractall(path = BUILD_DIR)
    subprocess.run(["patch","-p0","-d",BUILD_DIR],stdin = open("no_default_trusted.patch"))
    shutil.rmtree(os.path.join(BUILD_DIR,"META-INF"))
    version = link[len(LINK_PREFIX):-len(FILE_SUFFIX)].decode("ascii")
    filename = FILE_PREFIX + version + FILE_SUFFIX
    xpi = zipfile.ZipFile(filename,"w")
    for root, dirs, files in os.walk(BUILD_DIR):
        archive_root = os.path.relpath(root,start = BUILD_DIR)
        for x in files:
            xpi.write(os.path.join(root,x),arcname = os.path.join(archive_root,x))

if __name__ == "__main__":
    try:
        sys.exit(main())
    except KeyboardInterrupt:
        sys.exit(1)
